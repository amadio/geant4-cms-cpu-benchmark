# Geant4 Full CMS Benchmark

***22 November, 2017, Mihaly Novak***
***

This is a Geant4 CPU benchmark test based on the full CMS detector, imported via a `.gdml` file.

An installation of Geant4, including the GDML extension (which requires the XercesC package installed in the system) is required i.e. the Geant4 toolkit must be built with the `-DGEANT4_USE_GDML=ON` `CMake` option.


-----------
### DETECTOR


#### Detector construction:
The detector is imported from GDML file. Any GDML file can be set from the macro file by using the
```
   /mydet/setGdmlFile <gdml-file-name.gdml>
```
as long as generating events at the $`\mathbf{r}=[0,0,0]`$ position is suitable. The default value is `/cms2018.gdml`.

#### Magnetic filed:
A **constant** magnetic field can be set through the macro command
```
   /mydet/setField <field-value> <unit>
```
The default value is zero i.e. no magnetic field is created.

--------------------
### PRIMARY GENERATOR

The primary generator is a particle gun that will generate primary particles at the $`\mathbf{r}=[0,0,0]`$ position with the following options:

#### Number of primaries per event:
The number of primary particles per event can be set through the macro command
```
   /mygun/primaryPerEvt <number>
```
By default, i.e. if it's not specified by the above command, the **number of primary** particles will be **randomly selected** for each individual event from the `[1, 10]` interval uniformly.

#### Primary particle energy:
The primary particle energy can be set through the macro command
```
   /mygun/energy  <energy-value> <unit>
```
By default, i.e. if it is not specified by the above command, the **kinetic energy** will be **randomly selected** for each individual primary particle from the `[1 GeV, 100 GeV]` uniformly.

#### Primary particle direction:
The primary particle momentum direction can be set through the macro command
```
   /mygun/direction  <xdir-value> <ydir-value> <zdir-value>
```
By default, i.e. if it is not specified by the above command, the **momentum direction** will be **randomly selected** for each individual primary particles from isotropic distribution.

#### Primary particle type:
The primary particle type can be set through the macro command
```
   /mygun/particle <particle-name>
```
By default, i.e. if it is not specified by the above command, the **particle type** will be **randomly selected** from a pre-defined list for each individual **primary particle** uniformly. The current list of particles can be found in 
Table 

|Index|   G4 Name    | Particle |
|----:|:------------:|:--------:|
|  0 | mu-         | $`\mu^-`$    |
|  1 | mu+         | $`\mu^+`$    |
|  2 | e-          | $`e^-`$      |
|  3 | e+          | $`e^+`$      |
|  4 | gamma       | $`\gamma`$   |
|  5 | pi-         | $`\pi^-`$    |
|  6 | pi+         | $`\pi^+`$    |
|  7 | kaon-       | $`K^-`$      |
|  8 | kaon+       | $`K^+`$      |
|  9 | kaon0L      | $`K^0_L`$    |
| 10 | neutron     | $`n`$        |
| 11 | proton      | $`p/p^+`$    |
| 12 | anti_neutron| $`\bar{n}`$  |
| 13 | anti_proton | $`\bar{p}`$  |
| 14 | deuteron    | $`D/^2\!H`$  |
| 15 | triton      | $`^3\!H`$    |
| 16 | alpha       | $`\alpha/He^{2+}`$|
| 17 | lambda      | $`\Lambda`$  |
| 18 | sigma+      | $`\Sigma^+`$ |
| 19 | sigma-      | $`\Sigma^-`$ |
| 20 | xi-         | $`\Xi^-`$    |
| 21 | xi0         | $`\Xi^0`$    |
| 22 | anti_lambda | $`\bar{\Lambda}`$ |
| 23 | anti_sigma+ | $`\overline{\Sigma^+}`$|
| 24 | anti_sigma- | $`\overline{\Sigma^-}`$|
| 25 | anti_xi-    | $`\overline{\Xi^-}`$|
| 26 | anti_xi0    | $`\overline{\Xi^0}`$|
| 27 | omega-      | $`\Omega^-`$|
| 28 | anti_omega- | $`\overline{\Omega^-}`$|



------------------------
### PHYSICS LIST

Any of the the Geant4 reference physics lists can be used in the simulation. The physics list can be set as an input argument by providing its name after the `-f` flag (e.g. in order to use the `FTFP_BERT` physics list but with `option1 EM physics constructor` `-f FTFP_BERT_EMV`). Please note, that the name of the Geant4 built in physics list must be in upper case, exactly as the corresponding header file name. By default, i.e. if the physics list name is not provided as an input argument, the `FTFP_BERT` physics list (with the default `EM option0`) is used.

More details on the Geant4 reference physics lists and electromagnetic options can be found in the corresponding part of the [Geant4 documentation (Physics List Guide)](http://geant4-userdoc.web.cern.ch/geant4-userdoc/UsersGuides/PhysicsListGuide/html/index.html).



-------------------------
### BUILD, RUN and OPTIONS


The application can be built and used both with sequential and multithreaded Geant4 builds. In case of multithreaded Geant4 toolkit, the application will run in proper multithreaded mode. The number of worker threads can be controlled through the 
```
    /run/numberOfThreads <number-of-worker-thraeds>
```
macro command.

Run the executable with the `--help` input argument to see the available input argument options:

| Falg | Expected argument   |          |  Description |
|:----:|:-------------------:|:--------:|:-------------|
| `-m` |`<Geant4-Macro-File>`|`REQUIRED`|path to a standard Geant4 macro file |
| `-f` |`<Physics-List-Name>`|`OPTIONAL`|name of a Geant4 refernce physics list (default: `FTFP_BERT`) |
| `-p` |`<NO-ARGUMENT>`      |`OPTIONAL`|flag to run in performance mode i.e. without any user actions (default: `NO`)|

A minimal set of *observable* is collected during the simulation separately for each primary particle type: mean energy deposit, mean charged and neutral step lengths, mean number of steps made by charged and neutral particles, mean number of secondary $e^-, e^+$ and $\gamma$ particles. The result is reported at the end of each event for each primary particle that were transported in the given event.
At the end of the simulation a final report is printed showing the run time, the primary generator and magnetic field settings used during the run, the total number of events and primary particles transported and the per-primary type simulation statistics of the above-mentioned quantities.

The simulation can be executed in **performance mode** by providing the `-p` input flag. **No user actions** are created in this case beyond the only one RunAction (only for the Master-thread in case of MT) that will start and stop a timer at the beginning and the end of the simulation (initialisation time won't be included). Therefore, there is **no any scoring** in this case.


-------------------------
### EXAMPLE

To execute the application using the provided `bench.g4` macro file, by using the `FTFP_BERT` reference physics list with the `option1 EM physics constructor`
in performance mode:
```
   ./full_cms -m bench.g4 -f FTFP_BERT -p
```

------------------------
### FINAL REMARKS

To get the most performant Geant4 build, one needs to build the Geant4 toolkit with the following CMake options (in addition to the already mentioned `-DGEANT4_USE_GDML=ON`):
```
  -DGEANT4_BUILD_MULTITHREADED=ON       # build Geant4 libraries with support for multithreading
  -DGEANT4_BUILD_VERBOSE_CODE=OFF       # build Geant4 libraries without extra verbosity
  -DGEANT4_BUILD_STORE_TRAJECTORY=OFF   # do not store trajectories in event processing
```
preferably with the `-DBUILD_STATIC_LIBS=ON` option to obtain static Geant4 libraries. See more information on the above-mentioned Geant4 build options in the corresponding part of the [Geant4 documentation (Geant4 Installation Guide: Geant4 Build Options: Advanced Options)](http://geant4-userdoc.web.cern.ch/geant4-userdoc/UsersGuides/InstallationGuide/html/installguide.html#advanced-options).

Set the number of threads in the Geant4 macro file to the maximum available and execute the `full_cms` application in performance mode with the `-p` flag.
